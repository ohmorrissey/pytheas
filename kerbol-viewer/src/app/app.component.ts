import { Component, OnInit } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from "@three-ts/orbit-controls"
import { PlanetService } from './services/planet.service';
import { PLANETS } from 'configs/planet-config';

const DEBUG_MODE = true;

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  name = 'kerbol-viewer';
  scene = new THREE.Scene();
  time: number = 0;
  planetService = new PlanetService(PLANETS);
  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100000000);
  renderer = new THREE.WebGLRenderer();
  orbitControls = new OrbitControls(this.camera, this.renderer.domElement);
  debug: boolean = DEBUG_MODE;
  axis = new THREE.AxesHelper(0.5);


  ngOnInit() {

    this.camera.position.set(20000, 20000, 0);
    this.camera.rotation.set(0, Math.PI / 4, 0);
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    //Add the renderer canvas to the DOM.
    document.body.appendChild(this.renderer.domElement);

    //Create a new directional light
    const sunLight = new THREE.PointLight(0xfdfcf0, 1);
    sunLight.position.set(0, 0, 20000);
    this.scene.add(sunLight);

    const ambLight = new THREE.AmbientLight(0x888888, 0.3);
    this.scene.add(ambLight);

    this.axis.name = 'axis';
    this.camera.position

    const origin = new THREE.Vector3(0, 0, 0);
    const camDistToOrigin = this.camera.position.distanceTo(origin) / 20;
    this.axis.scale.set(camDistToOrigin, camDistToOrigin, camDistToOrigin);
    this.onClickDebug();

    this.planetService.renderStars(this.scene);
    this.planetService.renderBodies(this.scene);

    // //Cloud Geomtry and Material
    // const cloudGeometry = new THREE.SphereGeometry(5.1, 50, 50);
    // const cloudMaterial = new THREE.MeshPhongMaterial({
    //   map: new THREE.TextureLoader().load(this.CLOUD_PATH),
    //   transparent: true,
    //   opacity: 0.1
    // });

    // //Create a cloud mesh and add it to the scene.
    // var clouds = new THREE.Mesh(cloudGeometry, cloudMaterial);
    // scene.add(clouds);



    this.runAnimation(
      this.renderer,
      this.scene,
      this.planetService,
      this.time,
      this.camera,
      this.orbitControls,
      this.axis,
    );
  }

  /**
   * Calls the main animate loop for THREE.
   * @param renderer THREE.WebGLRenderer
   * @param scene THREE.Scene
   * @param planetService Planet Service
   * @param time Global time
   * @param camera THREE.PerspectiveCamera
   * @param orbitControls OrbitalControls
   */
  private runAnimation(
    renderer: any,
    scene: any,
    planetService: any,
    time: number,
    camera: any,
    orbitControls: OrbitControls,
    axis: any,
  ): void {

    // Listen for a window resize, then resize THREE renderer accordingly
    window.addEventListener('resize', WindowResize, false)

    /**
     * Resizes THREE scene to the full window.
     */
    function WindowResize() {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(window.innerWidth, window.innerHeight);
    }

    /**
     * Main loop for THREE animation
     */
    const animateTHREE = function () {
      planetService.animate(time);
      time += 1 / 60;

      const origin = new THREE.Vector3(0, 0, 0);
      const camDistToOrigin = camera.position.distanceTo(origin) / 5;
      axis.scale.set(camDistToOrigin, camDistToOrigin, camDistToOrigin);

      orbitControls.update();
      requestAnimationFrame(animateTHREE);
      renderer.render(scene, camera);
    };

    renderer.render(scene, camera);
    animateTHREE();
  }

  /**
   * Runs when console.log button pressed. Purely for clean debugging purposes.
   */
  onClickConsoleLog(): void {
    console.log(this.camera.position.x, this.camera.position.y, this.camera.position.z);
  }

  /**
   * runs on click of debug button
   */
  onClickDebug(): void {

    if (this.debug && !this.scene.getObjectByName('axis')) {
      this.scene.add(this.axis);
      console.log('Adding Axis');
    }
    if (!this.debug && this.scene.getObjectByName('axis')) {
      this.scene.remove(this.axis);
      console.log('Removing Axis');
    }

  }
}
