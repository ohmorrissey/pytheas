import { Inject, Injectable } from '@angular/core';
import * as THREE from 'three';
import {
  calcMeanAnomoly,
  calcEccentricAnomonly,
  eccentricAnomalyToCartesian,
  calcSemiMinorAxis
} from '../models/orrery'

import { IPlanetBody, IOrbit } from './../models/planet-body';
import { __values } from 'tslib';
import { Planet } from './planet';

const STAR_FIELD_MAP_PATH = 'https://i.imgur.com/kXirxkP.png'; // Original
//const STAR_FIELD_MAP_PATH = 'https://i.imgur.com/mw5K0Oq.jpg';

/**
 * Speed Scalar to make the animation faster for debugging
 */
const DEBUG_SCALAR: number = 10000;

@Injectable({
  providedIn: 'root'
})

/**
 * Handles the rendering of planets=
 */
export class PlanetService {

  constructor(@Inject(PlanetService)
  planets: IPlanetBody[]

  ) {
    for (let planetIn of planets) {
      const planet = new Planet(planetIn);
      this.planets.set(planet.id, planet)
      if (planet.moons !== null) {

        const moons = planet.moons;
        moons?.forEach(moon => {
          const planet = new Planet(moon);
          this.planets.set(planet.id, planet)
        });
      }

    }
  }

  /**
   * Map of Planets
   */
  planets = new Map<Number, Planet>();

  /**
   * THREE.Mesh of stars
   */
  starField: THREE.Mesh = new THREE.Mesh();

  /**
   * Renders inside out star map to THREE Scene
   * @param scene THREE Scene to add stars to
   */
  public renderStars(scene: THREE.Scene): void {
    const starGeometry = new THREE.SphereGeometry(1000000, 50, 50);
    const starMaterial = new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load(STAR_FIELD_MAP_PATH),
      side: THREE.BackSide,
      shininess: 0
    });
    this.starField = new THREE.Mesh(starGeometry, starMaterial);
    scene.add(this.starField);

  }

  /**
   * Calls a render function for each of the planets in the
   * config files
   * @param scene THREE.Scene to render bodies on
   */
  public renderBodies(scene: THREE.Scene): void {

    for (let [id, planet] of this.planets) {
      planet.renderBody(scene);
    }
  }


  /**
   * Animation loop for the rendered planets.
   * Called by the THREE animate loop.
   * @param time Global Time
   */
  public animate(time: number): void {
    time *= DEBUG_SCALAR;

    let offsetPosition = new THREE.Vector3(0, 0, 0);


    for (let [id, planet] of this.planets) {
      const parentPlanet = planet.getParent(this.planets);
      if (parentPlanet !== undefined) {
        offsetPosition = parentPlanet.position
      }
      planet.animate(time, offsetPosition);
    }
  }

}
