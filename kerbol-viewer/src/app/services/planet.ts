import { Inject, Injectable, OnInit } from "@angular/core";
import * as THREE from "three";
import { IOrbit, IPlanetBody } from "./../models/planet-body";
import {
  calcMeanAnomoly,
  calcEccentricAnomonly,
  eccentricAnomalyToCartesian,
  calcSemiMinorAxis
} from '../models/orrery'
import { Euler } from "three";



@Injectable({
  providedIn: 'root'
})
export class Planet {

  /**
   * Planet id
   */
  id: number;
  /**
   * Name of planet
   */
  name: string;
  /**
   * Planet characteristics
   */
  planetBody: IPlanetBody;

  mesh: THREE.Mesh | undefined;

  position: THREE.Vector3;

  rotation: THREE.Euler;

  moons = new Map<number, IPlanetBody>();
  /**
   * orbital characteristics
   */
  orbit: IOrbit


  constructor(@Inject(Planet) planetIn: IPlanetBody) {
    this.planetBody = planetIn;
    this.id = planetIn.id;
    this.name = planetIn.name;
    planetIn.moon?.forEach(moon =>
      this.moons.set(moon.id, moon)
    )
    this.position = new THREE.Vector3(0, 0, 0);
    this.rotation = new THREE.Euler(0, 0, 0);
    this.orbit = planetIn.orbit;
    this.orbit.semiMinor = calcSemiMinorAxis(
      this.planetBody.orbit.semiMajor,
      this.planetBody.orbit.eccentricity
    );
  };

  /**
   * Adds a planet to a THREE.Scene
   * @param scene THREE.Scene to render body on
   */
  public renderBody(scene: THREE.Scene): void {

    const bodyGeometry = new THREE.SphereGeometry(this.planetBody.radius, 50, 50);

    const bodyMaterial = new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load(this.planetBody.image.map),
      color: 0xaaaaaa,
      specular: 0x333333,
      shininess: 10
    });

    //Build earth mesh using our geometry and material
    this.mesh = new THREE.Mesh(bodyGeometry, bodyMaterial);

    //add the earth mesh to the scene
    scene.add(this.mesh);
  }

  /**
   * Returns the parent planet of the planet.
   * ie, moon.getParent(solarsystem) = earth
   * @param planets map of planets in the system
   * @returns parent planet or undefined for the highest parent
   */
  public getParent(planets: Map<Number, Planet>): Planet | undefined {

    let parent: Planet | undefined = undefined;

    for (let [id, planet] of planets) {
      planet.moons?.forEach((moon) => {
        if (moon.id === this.id) {
          parent = planet;
        }
      });
    }
    return parent
  }

  /**
   * Animation loop for the rendered planets.
   * Called by the THREE animate loop.
   * @param time Global Time
   */
  public animate(time: number, offsetPosition: THREE.Vector3): void {

    this.setPlanetPosition(time);
    this.position.add(offsetPosition);

    this.rotation.y = this.setPlanetRotation(time);

    this.mesh?.rotation.set(this.rotation.x, this.rotation.y, this.rotation.z);
    this.mesh?.position.set(this.position.x, this.position.y, this.position.z);
  }

  private setPlanetPosition(time: number): void {
    const meanAnomoly = calcMeanAnomoly(this.orbit.orbitPeriod, time);
    const eccentricAnomaly = calcEccentricAnomonly(meanAnomoly, this.orbit.eccentricity);
    const orbitPosition2D = eccentricAnomalyToCartesian(
      this.orbit.semiMajor,
      this.orbit.semiMinor,
      this.orbit.eccentricity,
      eccentricAnomaly
    );

    this.position = new THREE.Vector3(orbitPosition2D[0], 0, orbitPosition2D[1]);
    const periArgMatrix = new THREE.Euler(0, this.orbit.periArg * Math.PI / 180, 0);

    this.position.applyEuler(periArgMatrix);

    const inclinationMatrix = new THREE.Euler(this.orbit.inclination * Math.PI / 180, 0, 0);

    this.position.applyEuler(inclinationMatrix)

    const ascNodeLongMatrix = new THREE.Euler(0, this.orbit.ascNodeLong, 0);
    this.position.applyEuler(ascNodeLongMatrix);
  }

  /**
    * Calculates the planet angle from orbital data and global time.
    * @param time Global Time
    * @returns Angle of the planet (Radians)
    */
  private setPlanetRotation(time: number): number {
    let angle = this.periodConverter(time);
    return angle
  }

  /**
   * Calculates the value for trig functions based on orbital parameters
   * @param time Global Time
   * @returns angle for trig functions
   */
  private periodConverter(time: number): number {
    let angle = time * 2 * Math.PI / this.orbit.rotationPeriod;
    angle = angle % (2 * Math.PI);
    return angle;
  }

}
