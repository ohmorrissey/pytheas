
/**
 * Planet characteristics
 */
export interface IPlanetBody {
  /**
   * Planet id
   */
  id: number;
  /**
   * Name of planet
   */
  name: string;
  /**
   * Equatorial radius (km)
   */
  radius: number;
  /**
   * Set of image links for planet
   */
  image: IPlanetImage;
  /**
   * Orbital Characteristics
   */
  orbit: IOrbit;
  /**
   * Array of Moons within planet
   */
  moon?: IPlanetBody[];
}

/**
 * Orbital Characteristics of a IPlanetBody
 */
export interface IOrbit {
  /**
   * Semi Major Axis (km)
   */
  semiMajor: number,
  /**
   * Semi Minor Axis (km)
   */
  semiMinor: number,
  /**
   * Apogee height (km)
   */
  apoapsis: number,
  /**
   * Perigee height (km)
   */
  periapsis: number,
  /**
   * Eccentricity (deg)
   */
  eccentricity: number,
  /**
   * Inclination (deg)
   */
  inclination: number,
  /**
   * Argument of the Periapsis (deg)
   */
  periArg: number,
  /**
   * Longitute of the Ascending Node (deg)
   */
  ascNodeLong: number,
  /**
   * Mean anomoly (rad)
   */
  meanAnomoly: number,
  /**
   * Orbital Period relative to Global Coordinates (s)
   */
  orbitPeriod: number,
  /**
   * Planet rotation period relative to Global Coordinates (s)
   */
  rotationPeriod: number
}

/**
 * Set of images to source textures
 */
export interface IPlanetImage {
  /**
   * Planet Texture
   */
  map: string;
  /**
   * Cloud Texture
   */
  clouds: string;
  /**
   * Topology map
   */
  elevation: string;
}

