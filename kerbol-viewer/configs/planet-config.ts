import { IPlanetBody } from './../src/app/models/planet-body'

/**
 * Planet configuration from https://wiki.kerbalspaceprogram.com/wiki/
 */
export const PLANETS: IPlanetBody[] = [{
  id: 1,
  name: 'kerbin',
  radius: 600,
  image: {
    map: "https://i.imgur.com/hrRiidF.jpg",
    clouds: "https://i.imgur.com/eKiliYc.jpg",
    elevation: ""
  },
  orbit: {
    semiMajor: 6000, //13599840.3,
    semiMinor: 0,
    apoapsis: 13599840.3,
    periapsis: 13599840.3,
    eccentricity: 0,
    inclination: 0,
    periArg: 0,
    ascNodeLong: 0,
    meanAnomoly: 3.14,
    orbitPeriod: 920354, //5,
    rotationPeriod: 21549.425
  },
  moon: [
    {
      id: 2,
      name: "mun",
      radius: 200,
      image: {
        map: "https://i.imgur.com/T912hUF.png",
        clouds: "",
        elevation: "https://i.imgur.com/d3sbuPm.png"
      },
      orbit: {
        semiMajor: 12000,
        semiMinor: 0,
        apoapsis: 12000,
        periapsis: 12000,
        eccentricity: 0.9,
        inclination: 45,
        periArg: 90,
        ascNodeLong: 180,
        meanAnomoly: 1.7,
        orbitPeriod: 138984,
        rotationPeriod: 138984.38
      }
    },
    // {
    //   id: 3,
    //   name: 'minmus',
    //   radius: 60,
    //   image: {
    //     map: "https://i.imgur.com/uuP1kwY.png",
    //     clouds: "",
    //     elevation: "https://i.imgur.com/jhsttXj.png"
    //   },
    //   orbit: {
    //     semiMajor: 47000,
    //     semiMinor: 0,
    //     apoapsis: 47000,
    //     periapsis: 47000,
    //     eccentricity: 0,
    //     inclination: 6,
    //     periArg: 78,
    //     ascNodeLong: 78,
    //     meanAnomoly: 0.9,
    //     orbitPeriod: 1077311,
    //     rotationPeriod: 40400
    //   }
    // }
  ]
}]
