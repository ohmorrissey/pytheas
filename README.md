# Pytheas

Pytheas of Massalia was a Greek geographer, explorer and astronomer from the Greek colony of Massalia. 

Pytheas aims to bring Kerbal Space Program map mode to a browser for a second monitor or seperate PC.  

## Getting started

To get started with Pytheas, clone the repo and inside the repo, install the dependencies from the package.json.
```
npm run i #This will decent into the subfolders and run npm i for all required folders
```
### First)

This should include `lite-server`. There are two options to serve the web app. 
Directly with Angular
``` 
cd kerbol-viewer
ng serve --open
```
### Second)

With lite server that includes auto rebuild and reload
```
# on one terminal 
cd kerbol-viewer
ng build --watch

# on a second terminal 
cd kerbol-viewer
npm run dev
```
This should play the current app on a web browser. 

# Dependencies

``` 

# Install latest Node.js
# https://github.com/nodesource/distributions/blob/master/README.md

#Install latest npm
sudo apt install npm 

``` 

